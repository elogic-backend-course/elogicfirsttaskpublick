<?php
ini_set('include_path', __DIR__);

ini_set('display_errors', 0);
error_reporting(E_ERROR);

//error handler function
function customError($errno, $errstr) {
    echo "got some error [$errno] $errstr";
}

//set error handler
set_error_handler("customError");


use App\Geometry\Figure\Square\Paralelogram as MyFigure;

include_once 'vendor/autoload.php';

//echo ($test['test']/0);
//file_get_contents('test.txt');

$logger = new \App\Loger();

$obj = new MyFigure(10,5, 4);
//$obj->h = 11;
//echo $obj->h;

//$obj->setBasePrice(11);
//echo $obj->getBasePrice();


try {
    $newObj = clone $obj;

    $logger->logInfo($obj);
    $newObj->setH("");
    $newObj->setA(10);


    echo $obj->getH() . "\n";
    echo $newObj->getH() . "\n";
} catch (\App\Geometry\Figure\SideNotSetException $e) {
//    echo "catch SideNotSetException \n";
//    echo $e->getMessage() . " \n";
    $logger->logError($e->getMessage());
} catch (\App\Geometry\Figure\SideIsNegativeException $e) {
//    echo "catch SideIsNegativeException \n";
//    echo $e->getMessage() . " \n";
    $logger->logError($e->getMessage());
} catch (\Exception $e) {
//    echo "catch Exception \n";
//    echo $e->getMessage() . " \n";
    $logger->logError($e->getMessage());
} finally {
    echo "finaly \n";
}

echo "\n";
//$obj->b = 5;
//$obj->h = 4;

//var_dump($obj->getSquare());
//var_dump($obj->getPerimeter());
//var_dump($obj->getSquareAndPerimeter());