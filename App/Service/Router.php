<?php

namespace App\Service;

use App\Controller\Home;
use App\Controller\Paralelogram;
use App\Controller\Rectangle;
use App\Controller\Square;
use Klein\Klein;

class Router
{
    public static function dispatch()
    {
        $klein = new Klein();

        $klein->respond('GET', '/', function() {
            $controller = new Home();
            $controller->execute();
        });

        $klein->respond('GET', '/square', function() {
            $controller = new Square();
            $controller->execute();
        });

        $klein->respond('GET', '/rectangle', function() {
            $controller = new Rectangle();
            $controller->execute();
        });

        $klein->respond('GET', '/paralelogram', function() {
            $controller = new Paralelogram();
            $controller->execute();
        });

        $klein->dispatch();
    }
}