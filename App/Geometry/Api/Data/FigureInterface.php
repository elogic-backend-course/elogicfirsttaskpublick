<?php

namespace App\Geometry\Api\Data;

interface FigureInterface
{
    const FACTOR = 100;
    /**
     * Returns figure perimeter
     * @return float
     */
    public function getPerimeter();

    /**
     * Returns figure square
     * @return float
     */
    public function getSquare();
}