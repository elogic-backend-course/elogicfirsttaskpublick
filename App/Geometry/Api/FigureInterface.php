<?php

namespace App\Geometry\Api;

interface FigureInterface
{
    /**
     * calculate square and perimeter
     * @return array
     */
    public function getSquareAndPerimeter();
}