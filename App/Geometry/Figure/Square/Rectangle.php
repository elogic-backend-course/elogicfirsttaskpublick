<?php

namespace App\Geometry\Figure\Square;

/**
 * Class Rectangle
 */
class Rectangle extends Square
{
    /** @var float */
    protected $b;

    /**
     * Rectangle constructor.
     * @param float $a
     * @param float $b
     */
    public function __construct($a,$b)
    {
        parent::__construct($a);
        $this->b = $b;
    }


    /**
     * calculate Square
     * @return float
     */
    protected function calculateSquare()
    {
        return $this->a * $this->b;
    }

    /**
     * calculate perimeter
     * @return float
     */
    protected function calculatePerimeter()
    {
        return 2*$this->a + 2*$this->b;
    }
}