<?php

namespace App\Geometry\Figure\Square;

/**
 * Class Paralelogram
 * @package App\Geometry\Figure\Square
 */
class Paralelogram extends Rectangle
{
    /** @var float */
    protected $h;

    /**
     * Paralelogram constructor.
     * @param float $a
     * @param float $b
     * @param float $h
     */
    public function __construct($a, $b, $h = null)
    {
        parent::__construct($a, $b);
        $this->h = $h;
    }

    /**
     * calculate Square
     * @return float
     */
    protected function calculateSquare()
    {
        if (isset($this->h)) {
            return $this->a * $this->h;
        } else {
            return parent::calculateSquare();
        }
    }

    public function getSquareAndPerimeter()
    {
        return [
            "square"    => $this->calculateSquare(),
            "perimeter" => $this->calculatePerimeter()
        ];
    }

    public function __toString()
    {
        return "a: " . $this->a . " b: " . $this->b . "h: " . $this->h;
    }
}