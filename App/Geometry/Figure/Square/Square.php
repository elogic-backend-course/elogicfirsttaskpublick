<?php

namespace App\Geometry\Figure\Square;

class Square extends SquareFigureAbstract
{
    /**
     * Square constructor.
     * @param float $a
     */
    public function __construct($a)
    {
        $this->a = $a;
    }

    /**
     * calculate Square
     * @return float
     */
    protected function calculateSquare()
    {
        return $this->a * $this->a;
    }

    /**
     * calculate perimeter
     * @return float
     */
    protected function calculatePerimeter()
    {
        return 4*$this->a;
    }

    /**
     * @param float $a
     * @return float
     */
    public static function calculateAnySquareBySide($a)
    {
        return $a*$a;
    }
}