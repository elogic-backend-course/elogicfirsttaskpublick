<?php

namespace App\Geometry\Figure\Square;

use App\Geometry\Api\Data\FigureInterface as FigureDataInterface;
use App\Geometry\Api\FigureInterface as FigureInterface;
use App\Geometry\Figure\SideIsNegativeException;
use App\Geometry\Figure\SideNotSetException;

abstract class SquareFigureAbstract implements FigureInterface, FigureDataInterface
{
    /**
     * @var float
     */
    protected $a;

    /**
     * calculate Square
     * @return float
     */
    abstract protected function calculateSquare();

    /**
     * calculate perimeter
     * @return float
     */
    abstract protected function calculatePerimeter();

    /**
     * @inheritdoc
     */
    public function getSquareAndPerimeter()
    {
        return [
            "square"    => $this->calculateSquare(),
            "perimeter" => $this->calculatePerimeter()
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPerimeter()
    {
        return $this->calculatePerimeter();
    }

    public function getSquare()
    {
        return $this->calculateSquare();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        echo "get ". $name . "\n";
        return $this->$name;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        echo "set ". $name . " with value ". $value . "\n";
        $this->$name = $value;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        switch (substr($name,0,3)) {
            case 'get' :
                $var = strtolower(substr($name,3, 1)) . substr($name,4);
                return $this->$var;
                break;
            case 'set' :
                $var = strtolower(substr($name,3, 1)) . substr($name,4);
                if (empty($arguments[0])) {
                    throw new SideNotSetException("Side $var should not be empty");
                }
                if ($arguments[0] <= 0) {
                    throw new SideIsNegativeException("Side $var should be greater then 0");
                }
                $this->$var = $arguments[0];
                break;
            default :
                echo "not sure what to do \n";
        }
    }
}