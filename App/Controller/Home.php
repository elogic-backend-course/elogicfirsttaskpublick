<?php

namespace App\Controller;

class Home
{
    public function execute()
    {
        echo "<ul>";
        echo "<li><a href='/square'>Square</a></li>";
        echo "<li><a href='/rectangle'>Rectangle</a></li>";
        echo "<li><a href='/paralelogram'>Paralelogram</a></li>";
        echo "</ul>";
    }
}