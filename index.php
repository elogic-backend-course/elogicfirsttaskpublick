<?php
ini_set('include_path', __DIR__);

ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once 'vendor/autoload.php';

App\App::run();